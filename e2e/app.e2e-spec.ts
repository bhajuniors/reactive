import { ReactivePage } from './app.po';

describe('reactive App', () => {
  let page: ReactivePage;

  beforeEach(() => {
    page = new ReactivePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
