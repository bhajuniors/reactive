import { Component, OnInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  mouseMove: Observable<any>;

  constructor(private el: ElementRef) {    
    this.mouseMove = Observable.fromEvent(this.el.nativeElement, 'click');

  }

  ngOnInit() {
    this.mouseMove
    .map(m => m.clientX)
    .subscribe(event => {
      console.log(event);
    });
  }

}
