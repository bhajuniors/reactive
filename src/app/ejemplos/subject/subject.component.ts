import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent {
  service: ContactoService;
  contactos: Contacto[];
  nombre: string;
  eventos: Subscription;

  constructor() {
    this.service = new ContactoService();
    this.contactos = new Array<Contacto>();

    this.suscribir();

  }

  agregar() {
    this.service.agregar(this.nombre);
    this.nombre = "";
  }

  suscribir() {
    this.eventos = this.service.subject.subscribe(contacto => {
      console.log('NUEVO CONTACTO');
      this.contactos.push(contacto);
    })
  }
  
  cancelar() {
    this.eventos.unsubscribe();
  }

}

export class ContactoService {
  private contactos: Contacto[];
  public subject: Subject<Contacto>;

  constructor() {
    this.subject = new Subject<Contacto>();
    this.contactos = new Array<Contacto>();
  }

  agregar(nombre: string) {
    let contacto = new Contacto(nombre);
    this.contactos.push(contacto);
    this.subject.next(contacto);
  }
}

export class Contacto {
  constructor(public nombre: string) {

  }
}